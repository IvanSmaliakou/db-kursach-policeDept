﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    public partial class Squads_Form : Form
    {
        public Squads_Form()
        {
            InitializeComponent();
            squadNameTB.Visible = false;
            carNumberCB.Visible = false;
            squadNameLabel.Visible = false;
            licenseLabel.Visible = false;
            addSquadApplyButton.Visible = false;
        }

        private void Squads_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "patrolCarsDS.patrol_car". При необходимости она может быть перемещена или удалена.
            this.patrol_carTableAdapter.Fill(this.patrolCarsDS.patrol_car);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "fullSquadDataSet.full_squad". При необходимости она может быть перемещена или удалена.
            this.full_squadTableAdapter2.Fill(this.fullSquadDataSet.full_squad);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void addSquadButton_Click(object sender, EventArgs e)
        {
            squadNameTB.Visible = true;
            carNumberCB.Visible = true;
            squadNameLabel.Visible = true;
            licenseLabel.Visible = true;
            addSquadApplyButton.Visible = true;
        }

        private void addSquadApplyButton_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
               ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("insert_squad", conn);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter codeName = new SqlParameter("@code_name", squadNameTB.Text);
                SqlParameter license = new SqlParameter("@patrol_car_license", carNumberCB.Text);

                SqlParameter acc_id = new SqlParameter("@squad_id", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(codeName);
                command.Parameters.Add(license);
                command.Parameters.Add(acc_id);

                command.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show(String.Format("Наряд с кодовым названием \"{0}\" и id={1} был добавлен", squadNameTB.Text, acc_id.Value), "Внимание!",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            squadNameTB.Visible = false;
            carNumberCB.Visible = false;
            squadNameLabel.Visible = false;
            licenseLabel.Visible = false;
            addSquadApplyButton.Visible = false;
        }

        private void deleteSquadButton_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
               ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    SqlCommand cmd = new SqlCommand("delete from squad where code_name=@code_name", conn);
                    SqlParameter codeName = new SqlParameter("@code_name", row.Cells[0].Value as string);
                    cmd.Parameters.Add(codeName);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
                MessageBox.Show("Наряд был удален", "Внимание!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
