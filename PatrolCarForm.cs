﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    public partial class PatrolCarForm : Form
    {
        public PatrolCarForm()
        {
            InitializeComponent();
            modelLabel.Visible = false;
            yearLabel.Visible = false;
            licPlateLabel.Visible = false;
            carModelTB.Visible = false;
            yearTB.Visible = false;
            licenseTB.Visible = false;
            addCarApplyButton.Visible = false;
        }

        private void PatrolCarForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "patrolCarBindingSource1.patrol_car". При необходимости она может быть перемещена или удалена.
            this.patrol_carTableAdapter1.Fill(this.patrolCarBindingSource1.patrol_car);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "patrolCarDataSet.patrol_car". При необходимости она может быть перемещена или удалена.
            this.patrol_carTableAdapter.Fill(this.patrolCarDataSet.patrol_car);

        }

        private void addCarButton_Click(object sender, EventArgs e)
        {
            modelLabel.Visible = true;
            yearLabel.Visible = true;
            licPlateLabel.Visible = true;
            carModelTB.Visible = true;
            yearTB.Visible = true;
            licenseTB.Visible = true;
            addCarApplyButton.Visible = true;
        }

        private void addCarApplyButton_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
                ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("insert_patrol_car", conn);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter model = new SqlParameter("@model", carModelTB.Text);
                SqlParameter year = new SqlParameter("@year", Int32.Parse(yearTB.Text));
                SqlParameter license = new SqlParameter("@license_plate", licenseTB.Text);

                SqlParameter acc_id = new SqlParameter("@car_id", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(model);
                command.Parameters.Add(year);
                command.Parameters.Add(license);
                command.Parameters.Add(acc_id);

                command.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show(String.Format("Патрульная машина c номером \"{0}\" и id={1} была добавлена", licenseTB.Text, acc_id.Value), "Внимание!",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            modelLabel.Visible = false;
            yearLabel.Visible = false;
            licPlateLabel.Visible = false;
            carModelTB.Visible = false;
            yearTB.Visible = false;
            licenseTB.Visible = false;
            addCarApplyButton.Visible = false;
        }

        private void deleteCarButton_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
                ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    SqlCommand cmd = new SqlCommand("delete from patrol_car where license_plate=@lic_plate", conn);
                    SqlParameter licPlate = new SqlParameter("@lic_plate", row.Cells[1].Value as string);
                    cmd.Parameters.Add(licPlate);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
                MessageBox.Show("Патрульная машина была удалена", "Внимание!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
