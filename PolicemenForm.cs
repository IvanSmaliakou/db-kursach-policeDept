﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    public partial class PolicemenForm : Form
    {
        public PolicemenForm()
        {
            InitializeComponent();
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            surnameTB.Visible = false;
            nameTB.Visible = false;
            dateTimePicker1.Visible = false;
            squadNameCB.Visible = false;
            addMusorApplyBnt.Visible = false;
        }

        private void PolicemenForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "squadBindingSource.squad". При необходимости она может быть перемещена или удалена.
            this.squadTableAdapter.Fill(this.squadBindingSource.squad);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "policemanDS.policeman". При необходимости она может быть перемещена или удалена.
            this.policemanTableAdapter1.Fill(this.policemanDS.policeman);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "policemenDataSet.policeman". При необходимости она может быть перемещена или удалена.
            this.policemanTableAdapter.Fill(this.policemenDataSet.policeman);

        }

        private void buttonFilter_click(object sender, EventArgs e)
        {

        }

        private void addMusorBtn_Click(object sender, EventArgs e)
        {
            label1.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            surnameTB.Visible = true;
            nameTB.Visible = true;
            dateTimePicker1.Visible = true;
            squadNameCB.Visible = true;
            addMusorApplyBnt.Visible = true;


        }

        private void addMusorApplyBnt_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
              ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("insert_policeman", conn);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter name = new SqlParameter("@name", nameTB.Text);
                SqlParameter surname = new SqlParameter("@surname", surnameTB.Text);
                SqlParameter birthDate = new SqlParameter("@birth_date", dateTimePicker1.Value);
                SqlParameter squadCodeName = new SqlParameter("@squad_code_name", squadNameCB.Text);

                SqlParameter personalNumber = new SqlParameter("@personal_number", SqlDbType.VarChar)
                {
                    Direction = ParameterDirection.Output
                };
                personalNumber.Size = 5;
                SqlParameter acc_id = new SqlParameter("@id", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(name);
                command.Parameters.Add(surname);
                command.Parameters.Add(birthDate);
                command.Parameters.Add(squadCodeName);
                command.Parameters.Add(personalNumber);
                command.Parameters.Add(acc_id);

                command.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show(String.Format("Милиционер с личным номером \"{0}\" и id={1} был добавлен", personalNumber.Value as string, acc_id.Value), "Внимание!",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            surnameTB.Visible = false;
            nameTB.Visible = false;
            dateTimePicker1.Visible = false;
            squadNameCB.Visible = false;
            addMusorApplyBnt.Visible = false;
        }

        private void deleteMusorBtn_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
               ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    SqlCommand cmd = new SqlCommand("delete from policeman where personal_number=@personal_number", conn);
                    SqlParameter codeName = new SqlParameter("@personal_number", row.Cells[3].Value as string);
                    cmd.Parameters.Add(codeName);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
                MessageBox.Show("Милиционер был удален", "Внимание!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
