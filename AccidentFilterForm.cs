﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    public partial class AccidentFilterForm : Form
    {
        DataGridView parentDataGrid;
        BindingSource parentBs;
        public AccidentFilterForm(DataGridView dataGrid, BindingSource bs)
        {
            parentDataGrid = dataGrid;
            parentBs = bs;
            InitializeComponent();
            this.accTypeSelector.Items.AddRange(new object[] {
            "все"});
            string connString = System.Configuration.ConfigurationManager.
                   ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("SELECT name from accident_type", conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    this.accTypeSelector.Items.Add(reader.GetString(0));
                }
                conn.Close();
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void AccidentFilterForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "accTypeNamesDataSet.accident_type". При необходимости она может быть перемещена или удалена.
            this.accident_typeTableAdapter.Fill(this.accTypeNamesDataSet.accident_type);

        }

        private void buttonApplyFilter_Click(object sender, EventArgs e)
        {
            if (this.accTypeSelector.Text == "все" || this.accTypeSelector.Text == "")
            {
                parentBs.Filter = String.Format("acc_time >= '{0:yyyy-MM-dd HH:mm:ss}' and acc_time <= '{1:yyyy-MM-dd HH:mm:ss}'", this.filterDateTimePickerFrom.Value, this.filterDateTimePickerTo.Value);
            }
            else
            {
                parentBs.Filter = String.Format("acc_time >= '{0:yyyy-MM-dd HH:mm:ss}' and acc_time <= '{1:yyyy-MM-dd HH:mm:ss}' and acc_type='{2}'",
                    this.filterDateTimePickerFrom.Value, this.filterDateTimePickerTo.Value, this.accTypeSelector.Text);
            }
            this.Close();
        }

        private void accTypeSelector_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
