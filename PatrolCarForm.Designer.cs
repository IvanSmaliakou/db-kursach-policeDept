﻿
namespace Kursach_PoliceDept
{
    partial class PatrolCarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatrolCarForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.modelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.licenseplateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patrolcarBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.patrolCarBindingSource1 = new Kursach_PoliceDept.patrolCarBindingSource();
            this.patrolcarBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.patrolCarDataSet = new Kursach_PoliceDept.PatrolCarDataSet();
            this.patrol_carTableAdapter = new Kursach_PoliceDept.PatrolCarDataSetTableAdapters.patrol_carTableAdapter();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.patrol_carTableAdapter1 = new Kursach_PoliceDept.patrolCarBindingSourceTableAdapters.patrol_carTableAdapter();
            this.addCarApplyButton = new System.Windows.Forms.Button();
            this.addCarButton = new System.Windows.Forms.Button();
            this.deleteCarButton = new System.Windows.Forms.Button();
            this.carModelTB = new System.Windows.Forms.TextBox();
            this.modelLabel = new System.Windows.Forms.Label();
            this.yearTB = new System.Windows.Forms.TextBox();
            this.yearLabel = new System.Windows.Forms.Label();
            this.licenseTB = new System.Windows.Forms.TextBox();
            this.licPlateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolcarBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolCarBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolcarBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolCarDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.modelDataGridViewTextBoxColumn,
            this.licenseplateDataGridViewTextBoxColumn,
            this.yearDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.patrolcarBindingSource2;
            this.dataGridView1.Location = new System.Drawing.Point(81, 127);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(622, 150);
            this.dataGridView1.TabIndex = 0;
            // 
            // modelDataGridViewTextBoxColumn
            // 
            this.modelDataGridViewTextBoxColumn.DataPropertyName = "model";
            this.modelDataGridViewTextBoxColumn.HeaderText = "модель машины";
            this.modelDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.modelDataGridViewTextBoxColumn.Name = "modelDataGridViewTextBoxColumn";
            this.modelDataGridViewTextBoxColumn.Width = 125;
            // 
            // licenseplateDataGridViewTextBoxColumn
            // 
            this.licenseplateDataGridViewTextBoxColumn.DataPropertyName = "license_plate";
            this.licenseplateDataGridViewTextBoxColumn.HeaderText = "номер";
            this.licenseplateDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.licenseplateDataGridViewTextBoxColumn.Name = "licenseplateDataGridViewTextBoxColumn";
            this.licenseplateDataGridViewTextBoxColumn.Width = 125;
            // 
            // yearDataGridViewTextBoxColumn
            // 
            this.yearDataGridViewTextBoxColumn.DataPropertyName = "year";
            this.yearDataGridViewTextBoxColumn.HeaderText = "год выпуска";
            this.yearDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.yearDataGridViewTextBoxColumn.Name = "yearDataGridViewTextBoxColumn";
            this.yearDataGridViewTextBoxColumn.Width = 125;
            // 
            // patrolcarBindingSource2
            // 
            this.patrolcarBindingSource2.DataMember = "patrol_car";
            this.patrolcarBindingSource2.DataSource = this.patrolCarBindingSource1;
            // 
            // patrolCarBindingSource1
            // 
            this.patrolCarBindingSource1.DataSetName = "patrolCarBindingSource";
            this.patrolCarBindingSource1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // patrolcarBindingSource
            // 
            this.patrolcarBindingSource.DataMember = "patrol_car";
            this.patrolcarBindingSource.DataSource = this.patrolCarDataSet;
            // 
            // patrolCarDataSet
            // 
            this.patrolCarDataSet.DataSetName = "PatrolCarDataSet";
            this.patrolCarDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // patrol_carTableAdapter
            // 
            this.patrol_carTableAdapter.ClearBeforeFill = true;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.BindingSource = this.patrolcarBindingSource2;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(806, 27);
            this.bindingNavigator1.TabIndex = 1;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(55, 24);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // patrol_carTableAdapter1
            // 
            this.patrol_carTableAdapter1.ClearBeforeFill = true;
            // 
            // addCarApplyButton
            // 
            this.addCarApplyButton.Location = new System.Drawing.Point(148, 461);
            this.addCarApplyButton.Name = "addCarApplyButton";
            this.addCarApplyButton.Size = new System.Drawing.Size(82, 23);
            this.addCarApplyButton.TabIndex = 2;
            this.addCarApplyButton.Text = "добавить";
            this.addCarApplyButton.UseVisualStyleBackColor = true;
            this.addCarApplyButton.Click += new System.EventHandler(this.addCarApplyButton_Click);
            // 
            // addCarButton
            // 
            this.addCarButton.Location = new System.Drawing.Point(113, 316);
            this.addCarButton.Name = "addCarButton";
            this.addCarButton.Size = new System.Drawing.Size(171, 39);
            this.addCarButton.TabIndex = 3;
            this.addCarButton.Text = "Добавить машину";
            this.addCarButton.UseVisualStyleBackColor = true;
            this.addCarButton.Click += new System.EventHandler(this.addCarButton_Click);
            // 
            // deleteCarButton
            // 
            this.deleteCarButton.Location = new System.Drawing.Point(532, 316);
            this.deleteCarButton.Name = "deleteCarButton";
            this.deleteCarButton.Size = new System.Drawing.Size(171, 39);
            this.deleteCarButton.TabIndex = 4;
            this.deleteCarButton.Text = "Удалить машину";
            this.deleteCarButton.UseVisualStyleBackColor = true;
            this.deleteCarButton.Click += new System.EventHandler(this.deleteCarButton_Click);
            // 
            // carModelTB
            // 
            this.carModelTB.Location = new System.Drawing.Point(113, 361);
            this.carModelTB.Name = "carModelTB";
            this.carModelTB.Size = new System.Drawing.Size(171, 22);
            this.carModelTB.TabIndex = 5;
            // 
            // modelLabel
            // 
            this.modelLabel.AutoSize = true;
            this.modelLabel.Location = new System.Drawing.Point(13, 361);
            this.modelLabel.Name = "modelLabel";
            this.modelLabel.Size = new System.Drawing.Size(58, 17);
            this.modelLabel.TabIndex = 6;
            this.modelLabel.Text = "Модель";
            // 
            // yearTB
            // 
            this.yearTB.Location = new System.Drawing.Point(113, 395);
            this.yearTB.Name = "yearTB";
            this.yearTB.Size = new System.Drawing.Size(171, 22);
            this.yearTB.TabIndex = 7;
            // 
            // yearLabel
            // 
            this.yearLabel.AutoSize = true;
            this.yearLabel.Location = new System.Drawing.Point(12, 395);
            this.yearLabel.Name = "yearLabel";
            this.yearLabel.Size = new System.Drawing.Size(90, 17);
            this.yearLabel.TabIndex = 8;
            this.yearLabel.Text = "Год выпуска";
            // 
            // licenseTB
            // 
            this.licenseTB.Location = new System.Drawing.Point(113, 433);
            this.licenseTB.Name = "licenseTB";
            this.licenseTB.Size = new System.Drawing.Size(171, 22);
            this.licenseTB.TabIndex = 9;
            // 
            // licPlateLabel
            // 
            this.licPlateLabel.AutoSize = true;
            this.licPlateLabel.Location = new System.Drawing.Point(12, 433);
            this.licPlateLabel.Name = "licPlateLabel";
            this.licPlateLabel.Size = new System.Drawing.Size(80, 17);
            this.licPlateLabel.TabIndex = 10;
            this.licPlateLabel.Text = "Гос. номер";
            // 
            // PatrolCarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 525);
            this.Controls.Add(this.licPlateLabel);
            this.Controls.Add(this.licenseTB);
            this.Controls.Add(this.yearLabel);
            this.Controls.Add(this.yearTB);
            this.Controls.Add(this.modelLabel);
            this.Controls.Add(this.carModelTB);
            this.Controls.Add(this.deleteCarButton);
            this.Controls.Add(this.addCarButton);
            this.Controls.Add(this.addCarApplyButton);
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "PatrolCarForm";
            this.Text = "Патрульные машины";
            this.Load += new System.EventHandler(this.PatrolCarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolcarBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolCarBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolcarBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolCarDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private PatrolCarDataSet patrolCarDataSet;
        private System.Windows.Forms.BindingSource patrolcarBindingSource;
        private PatrolCarDataSetTableAdapters.patrol_carTableAdapter patrol_carTableAdapter;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private patrolCarBindingSource patrolCarBindingSource1;
        private System.Windows.Forms.BindingSource patrolcarBindingSource2;
        private patrolCarBindingSourceTableAdapters.patrol_carTableAdapter patrol_carTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn licenseplateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yearDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button addCarApplyButton;
        private System.Windows.Forms.Button addCarButton;
        private System.Windows.Forms.Button deleteCarButton;
        private System.Windows.Forms.TextBox carModelTB;
        private System.Windows.Forms.Label modelLabel;
        private System.Windows.Forms.TextBox yearTB;
        private System.Windows.Forms.Label yearLabel;
        private System.Windows.Forms.TextBox licenseTB;
        private System.Windows.Forms.Label licPlateLabel;
    }
}