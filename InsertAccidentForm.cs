﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kursach_PoliceDept
{
    public partial class InsertAccidentForm : Form
    {
        AccidentsForm parentForm;
        public InsertAccidentForm(AccidentsForm form)
        {
            InitializeComponent();
            this.parentForm = form;
        }

        private void InsertAccidentForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "squadCodeName.squad". При необходимости она может быть перемещена или удалена.
            this.squadTableAdapter.Fill(this.squadCodeName.squad);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "accidentTypes.accident_type". При необходимости она может быть перемещена или удалена.
            this.accident_typeTableAdapter.Fill(this.accidentTypes.accident_type);

        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
               ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString)) {
                conn.Open();
                SqlCommand command = new SqlCommand("insert_accident", conn);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter description = new SqlParameter("@description", accDescriptionTextBox.Text);
                SqlParameter address = new SqlParameter("@address", addressTextBox.Text);
                SqlParameter time = new SqlParameter("@time", datePicker.Value + timePicker.Value.TimeOfDay);
                SqlParameter type = new SqlParameter("@type_name", typePicker.Text);
                SqlParameter squadCode = new SqlParameter("@squad_code_name", squadCodeNamePicker.Text);
                SqlParameter acc_id = new SqlParameter("@acc_id", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(description);
                command.Parameters.Add(address);
                command.Parameters.Add(time);
                command.Parameters.Add(type);
                command.Parameters.Add(squadCode);
                command.Parameters.Add(acc_id);

                command.ExecuteNonQuery();
                conn.Close();
            }
            this.parentForm.accidentsDataGridView.Update();
            this.parentForm.accidentsDataGridView.Refresh();

            this.Close();
        }

        private void codeNamePicker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
