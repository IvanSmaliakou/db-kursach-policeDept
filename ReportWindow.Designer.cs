﻿
namespace Kursach_PoliceDept
{
    partial class ReportWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.fullAccidentReportDataSet = new Kursach_PoliceDept.fullAccidentReportDataSet();
            this.full_accidentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.full_accidentTableAdapter = new Kursach_PoliceDept.fullAccidentReportDataSetTableAdapters.full_accidentTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.fullAccidentReportDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_accidentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Accidents";
            reportDataSource1.Value = this.full_accidentBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Kursach_PoliceDept.ReportViewer.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(800, 450);
            this.reportViewer1.TabIndex = 0;
            // 
            // fullAccidentReportDataSet
            // 
            this.fullAccidentReportDataSet.DataSetName = "fullAccidentReportDataSet";
            this.fullAccidentReportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // full_accidentBindingSource
            // 
            this.full_accidentBindingSource.DataMember = "full_accident";
            this.full_accidentBindingSource.DataSource = this.fullAccidentReportDataSet;
            // 
            // full_accidentTableAdapter
            // 
            this.full_accidentTableAdapter.ClearBeforeFill = true;
            // 
            // ReportWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.reportViewer1);
            this.Name = "ReportWindow";
            this.Text = "Отчет о происшествии";
            this.Load += new System.EventHandler(this.ReportWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fullAccidentReportDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_accidentBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource full_accidentBindingSource;
        private fullAccidentReportDataSet fullAccidentReportDataSet;
        private fullAccidentReportDataSetTableAdapters.full_accidentTableAdapter full_accidentTableAdapter;
    }
}