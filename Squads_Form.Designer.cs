﻿
namespace Kursach_PoliceDept
{
    partial class Squads_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Squads_Form));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.squad_code_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patrol_car_lic_plate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patrol_car_model = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullsquadBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.fullSquadDataSet = new Kursach_PoliceDept.fullSquadDataSet();
            this.fullsquadBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.full_squadDataSet = new Kursach_PoliceDept.full_squadDataSet();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.full_squadTableAdapter = new Kursach_PoliceDept.police_deptDataSet5TableAdapters.full_squadTableAdapter();
            this.full_squadTableAdapter1 = new Kursach_PoliceDept.full_squadDataSetTableAdapters.full_squadTableAdapter();
            this.full_squadTableAdapter2 = new Kursach_PoliceDept.fullSquadDataSetTableAdapters.full_squadTableAdapter();
            this.squadBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fullsquadBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.squadBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.addSquadButton = new System.Windows.Forms.Button();
            this.addSquadApplyButton = new System.Windows.Forms.Button();
            this.deleteSquadButton = new System.Windows.Forms.Button();
            this.squadNameTB = new System.Windows.Forms.TextBox();
            this.squadNameLabel = new System.Windows.Forms.Label();
            this.licenseLabel = new System.Windows.Forms.Label();
            this.carNumberCB = new System.Windows.Forms.ComboBox();
            this.patrolCarsDS = new Kursach_PoliceDept.patrolCarsDS();
            this.patrolcarBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.patrol_carTableAdapter = new Kursach_PoliceDept.patrolCarsDSTableAdapters.patrol_carTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullsquadBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullSquadDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullsquadBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_squadDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.squadBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullsquadBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.squadBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolCarsDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolcarBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.squad_code_name,
            this.dataGridViewTextBoxColumn5,
            this.patrol_car_lic_plate,
            this.patrol_car_model});
            this.dataGridView1.DataSource = this.fullsquadBindingSource2;
            this.dataGridView1.Location = new System.Drawing.Point(98, 85);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(938, 255);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // squad_code_name
            // 
            this.squad_code_name.DataPropertyName = "squad_code_name";
            this.squad_code_name.HeaderText = "кодовое название наряда";
            this.squad_code_name.MinimumWidth = 6;
            this.squad_code_name.Name = "squad_code_name";
            this.squad_code_name.Width = 125;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "patrol_car_year";
            this.dataGridViewTextBoxColumn5.HeaderText = "год выпуска патрульной машины";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // patrol_car_lic_plate
            // 
            this.patrol_car_lic_plate.DataPropertyName = "patrol_car_lic_plate";
            this.patrol_car_lic_plate.HeaderText = "номер патрульной машины";
            this.patrol_car_lic_plate.MinimumWidth = 6;
            this.patrol_car_lic_plate.Name = "patrol_car_lic_plate";
            this.patrol_car_lic_plate.Width = 125;
            // 
            // patrol_car_model
            // 
            this.patrol_car_model.DataPropertyName = "patrol_car_model";
            this.patrol_car_model.HeaderText = "модель патрульной машины";
            this.patrol_car_model.MinimumWidth = 6;
            this.patrol_car_model.Name = "patrol_car_model";
            this.patrol_car_model.Width = 125;
            // 
            // fullsquadBindingSource2
            // 
            this.fullsquadBindingSource2.DataMember = "full_squad";
            this.fullsquadBindingSource2.DataSource = this.fullSquadDataSet;
            // 
            // fullSquadDataSet
            // 
            this.fullSquadDataSet.DataSetName = "fullSquadDataSet";
            this.fullSquadDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fullsquadBindingSource1
            // 
            this.fullsquadBindingSource1.DataMember = "full_squad";
            this.fullsquadBindingSource1.DataSource = this.full_squadDataSet;
            // 
            // full_squadDataSet
            // 
            this.full_squadDataSet.DataSetName = "full_squadDataSet";
            this.full_squadDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.BindingSource = this.fullsquadBindingSource2;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(1239, 27);
            this.bindingNavigator1.TabIndex = 1;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(55, 24);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // full_squadTableAdapter
            // 
            this.full_squadTableAdapter.ClearBeforeFill = true;
            // 
            // full_squadTableAdapter1
            // 
            this.full_squadTableAdapter1.ClearBeforeFill = true;
            // 
            // full_squadTableAdapter2
            // 
            this.full_squadTableAdapter2.ClearBeforeFill = true;
            // 
            // squadBindingSource
            // 
            this.squadBindingSource.DataMember = "squad";
            // 
            // addSquadButton
            // 
            this.addSquadButton.Location = new System.Drawing.Point(205, 385);
            this.addSquadButton.Name = "addSquadButton";
            this.addSquadButton.Size = new System.Drawing.Size(163, 33);
            this.addSquadButton.TabIndex = 2;
            this.addSquadButton.Text = "Добавить наряд";
            this.addSquadButton.UseVisualStyleBackColor = true;
            this.addSquadButton.Click += new System.EventHandler(this.addSquadButton_Click);
            // 
            // addSquadApplyButton
            // 
            this.addSquadApplyButton.Location = new System.Drawing.Point(240, 522);
            this.addSquadApplyButton.Name = "addSquadApplyButton";
            this.addSquadApplyButton.Size = new System.Drawing.Size(92, 24);
            this.addSquadApplyButton.TabIndex = 3;
            this.addSquadApplyButton.Text = "Добавить";
            this.addSquadApplyButton.UseVisualStyleBackColor = true;
            this.addSquadApplyButton.Click += new System.EventHandler(this.addSquadApplyButton_Click);
            // 
            // deleteSquadButton
            // 
            this.deleteSquadButton.Location = new System.Drawing.Point(873, 385);
            this.deleteSquadButton.Name = "deleteSquadButton";
            this.deleteSquadButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.deleteSquadButton.Size = new System.Drawing.Size(163, 33);
            this.deleteSquadButton.TabIndex = 4;
            this.deleteSquadButton.Text = "Удалить наряд";
            this.deleteSquadButton.UseVisualStyleBackColor = true;
            this.deleteSquadButton.Click += new System.EventHandler(this.deleteSquadButton_Click);
            // 
            // squadNameTB
            // 
            this.squadNameTB.Location = new System.Drawing.Point(215, 426);
            this.squadNameTB.Name = "squadNameTB";
            this.squadNameTB.Size = new System.Drawing.Size(141, 22);
            this.squadNameTB.TabIndex = 7;
            // 
            // squadNameLabel
            // 
            this.squadNameLabel.AutoSize = true;
            this.squadNameLabel.Location = new System.Drawing.Point(12, 429);
            this.squadNameLabel.Name = "squadNameLabel";
            this.squadNameLabel.Size = new System.Drawing.Size(182, 17);
            this.squadNameLabel.TabIndex = 8;
            this.squadNameLabel.Text = "Кодовое название наряда";
            // 
            // licenseLabel
            // 
            this.licenseLabel.AutoSize = true;
            this.licenseLabel.Location = new System.Drawing.Point(12, 472);
            this.licenseLabel.Name = "licenseLabel";
            this.licenseLabel.Size = new System.Drawing.Size(190, 17);
            this.licenseLabel.TabIndex = 9;
            this.licenseLabel.Text = "Номер патрульной машины";
            // 
            // carNumberCB
            // 
            this.carNumberCB.DataSource = this.patrolcarBindingSource;
            this.carNumberCB.DisplayMember = "license_plate";
            this.carNumberCB.FormattingEnabled = true;
            this.carNumberCB.Location = new System.Drawing.Point(215, 469);
            this.carNumberCB.Name = "carNumberCB";
            this.carNumberCB.Size = new System.Drawing.Size(141, 24);
            this.carNumberCB.TabIndex = 10;
            // 
            // patrolCarsDS
            // 
            this.patrolCarsDS.DataSetName = "patrolCarsDS";
            this.patrolCarsDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // patrolcarBindingSource
            // 
            this.patrolcarBindingSource.DataMember = "patrol_car";
            this.patrolcarBindingSource.DataSource = this.patrolCarsDS;
            // 
            // patrol_carTableAdapter
            // 
            this.patrol_carTableAdapter.ClearBeforeFill = true;
            // 
            // Squads_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 615);
            this.Controls.Add(this.carNumberCB);
            this.Controls.Add(this.licenseLabel);
            this.Controls.Add(this.squadNameLabel);
            this.Controls.Add(this.squadNameTB);
            this.Controls.Add(this.deleteSquadButton);
            this.Controls.Add(this.addSquadApplyButton);
            this.Controls.Add(this.addSquadButton);
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Squads_Form";
            this.Text = "Наряды";
            this.Load += new System.EventHandler(this.Squads_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullsquadBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullSquadDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullsquadBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_squadDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.squadBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullsquadBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.squadBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolCarsDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patrolcarBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource squadBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn patrolcaridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource squadBindingSource1;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.BindingSource fullsquadBindingSource;
        private police_deptDataSet5TableAdapters.full_squadTableAdapter full_squadTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn squadidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn patrolcarmodelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn patrolcaryearDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policemanidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policemannameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policemansurnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policemanbirthDataGridViewTextBoxColumn;
        private full_squadDataSet full_squadDataSet;
        private System.Windows.Forms.BindingSource fullsquadBindingSource1;
        private full_squadDataSetTableAdapters.full_squadTableAdapter full_squadTableAdapter1;
        private fullSquadDataSet fullSquadDataSet;
        private System.Windows.Forms.BindingSource fullsquadBindingSource2;
        private fullSquadDataSetTableAdapters.full_squadTableAdapter full_squadTableAdapter2;
        private System.Windows.Forms.DataGridViewTextBoxColumn squad_code_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn patrol_car_lic_plate;
        private System.Windows.Forms.DataGridViewTextBoxColumn patrol_car_model;
        private System.Windows.Forms.Button addSquadButton;
        private System.Windows.Forms.Button addSquadApplyButton;
        private System.Windows.Forms.Button deleteSquadButton;
        private System.Windows.Forms.Label squadNameLabel;
        private System.Windows.Forms.Label licenseLabel;
        private System.Windows.Forms.ComboBox carNumberCB;
        private patrolCarsDS patrolCarsDS;
        private System.Windows.Forms.BindingSource patrolcarBindingSource;
        private patrolCarsDSTableAdapters.patrol_carTableAdapter patrol_carTableAdapter;
        private System.Windows.Forms.TextBox squadNameTB;
    }
}