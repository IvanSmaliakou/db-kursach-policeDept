﻿
namespace Kursach_PoliceDept
{
    partial class InitialForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_show_accidents = new System.Windows.Forms.Button();
            this.button_show_types = new System.Windows.Forms.Button();
            this.button_show_squads = new System.Windows.Forms.Button();
            this.button_show_policemen = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button_show_patrol_cars = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_show_accidents
            // 
            this.button_show_accidents.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_show_accidents.Location = new System.Drawing.Point(382, 124);
            this.button_show_accidents.Name = "button_show_accidents";
            this.button_show_accidents.Size = new System.Drawing.Size(246, 53);
            this.button_show_accidents.TabIndex = 0;
            this.button_show_accidents.Text = "Происшествия";
            this.button_show_accidents.UseVisualStyleBackColor = true;
            this.button_show_accidents.Click += new System.EventHandler(this.button_show_accidents_Click);
            // 
            // button_show_types
            // 
            this.button_show_types.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_show_types.Location = new System.Drawing.Point(382, 202);
            this.button_show_types.Name = "button_show_types";
            this.button_show_types.Size = new System.Drawing.Size(246, 55);
            this.button_show_types.TabIndex = 1;
            this.button_show_types.Text = "Типы происшествий";
            this.button_show_types.UseVisualStyleBackColor = true;
            this.button_show_types.Click += new System.EventHandler(this.button_show_types_Click);
            // 
            // button_show_squads
            // 
            this.button_show_squads.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_show_squads.Location = new System.Drawing.Point(382, 286);
            this.button_show_squads.Name = "button_show_squads";
            this.button_show_squads.Size = new System.Drawing.Size(246, 55);
            this.button_show_squads.TabIndex = 2;
            this.button_show_squads.Text = "Наряды";
            this.button_show_squads.UseVisualStyleBackColor = true;
            this.button_show_squads.Click += new System.EventHandler(this.button_show_squads_Click);
            // 
            // button_show_policemen
            // 
            this.button_show_policemen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_show_policemen.Location = new System.Drawing.Point(382, 370);
            this.button_show_policemen.Name = "button_show_policemen";
            this.button_show_policemen.Size = new System.Drawing.Size(246, 55);
            this.button_show_policemen.TabIndex = 3;
            this.button_show_policemen.Text = "Милиционеры";
            this.button_show_policemen.UseVisualStyleBackColor = true;
            this.button_show_policemen.Click += new System.EventHandler(this.button_show_policemen_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(253, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(573, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Информационная система учета происшествий";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button_show_patrol_cars
            // 
            this.button_show_patrol_cars.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_show_patrol_cars.Location = new System.Drawing.Point(382, 463);
            this.button_show_patrol_cars.Name = "button_show_patrol_cars";
            this.button_show_patrol_cars.Size = new System.Drawing.Size(246, 55);
            this.button_show_patrol_cars.TabIndex = 5;
            this.button_show_patrol_cars.Text = "Патрульные машины";
            this.button_show_patrol_cars.UseVisualStyleBackColor = true;
            this.button_show_patrol_cars.Click += new System.EventHandler(this.button_show_patrol_cars_Click);
            // 
            // InitialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 642);
            this.Controls.Add(this.button_show_patrol_cars);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_show_policemen);
            this.Controls.Add(this.button_show_squads);
            this.Controls.Add(this.button_show_types);
            this.Controls.Add(this.button_show_accidents);
            this.Name = "InitialForm";
            this.Text = "Cистема учета происшествий";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_show_accidents;
        private System.Windows.Forms.Button button_show_types;
        private System.Windows.Forms.Button button_show_squads;
        private System.Windows.Forms.Button button_show_policemen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_show_patrol_cars;
    }
}

