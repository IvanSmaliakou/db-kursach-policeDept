﻿
namespace Kursach_PoliceDept
{
    partial class InsertAccidentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InsertAccidentForm));
            this.addButton = new System.Windows.Forms.Button();
            this.typePicker = new System.Windows.Forms.ComboBox();
            this.accidenttypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accidentTypes = new Kursach_PoliceDept.accidentTypes();
            this.accident_typeTableAdapter = new Kursach_PoliceDept.accidentTypesTableAdapters.accident_typeTableAdapter();
            this.squadCodeNamePicker = new System.Windows.Forms.ComboBox();
            this.squadBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.squadCodeName = new Kursach_PoliceDept.squadCodeName();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.accDescriptionTextBox = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.addressTextBox = new System.Windows.Forms.RichTextBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.squadTableAdapter = new Kursach_PoliceDept.squadCodeNameTableAdapters.squadTableAdapter();
            this.label5 = new System.Windows.Forms.Label();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.timePicker = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.accidenttypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.squadBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.squadCodeName)).BeginInit();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.Lime;
            resources.ApplyResources(this.addButton, "addButton");
            this.addButton.Name = "addButton";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // typePicker
            // 
            this.typePicker.DataSource = this.accidenttypeBindingSource;
            this.typePicker.DisplayMember = "name";
            this.typePicker.FormattingEnabled = true;
            resources.ApplyResources(this.typePicker, "typePicker");
            this.typePicker.Name = "typePicker";
            // 
            // accidenttypeBindingSource
            // 
            this.accidenttypeBindingSource.DataMember = "accident_type";
            this.accidenttypeBindingSource.DataSource = this.accidentTypes;
            // 
            // accidentTypes
            // 
            this.accidentTypes.DataSetName = "accidentTypes";
            this.accidentTypes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // accident_typeTableAdapter
            // 
            this.accident_typeTableAdapter.ClearBeforeFill = true;
            // 
            // squadCodeNamePicker
            // 
            this.squadCodeNamePicker.DataSource = this.squadBindingSource;
            this.squadCodeNamePicker.DisplayMember = "code_name";
            this.squadCodeNamePicker.FormattingEnabled = true;
            resources.ApplyResources(this.squadCodeNamePicker, "squadCodeNamePicker");
            this.squadCodeNamePicker.Name = "squadCodeNamePicker";
            this.squadCodeNamePicker.SelectedIndexChanged += new System.EventHandler(this.codeNamePicker_SelectedIndexChanged);
            // 
            // squadBindingSource
            // 
            this.squadBindingSource.DataMember = "squad";
            this.squadBindingSource.DataSource = this.squadCodeName;
            // 
            // squadCodeName
            // 
            this.squadCodeName.DataSetName = "squadCodeName";
            this.squadCodeName.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // accDescriptionTextBox
            // 
            resources.ApplyResources(this.accDescriptionTextBox, "accDescriptionTextBox");
            this.accDescriptionTextBox.Name = "accDescriptionTextBox";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // addressTextBox
            // 
            resources.ApplyResources(this.addressTextBox, "addressTextBox");
            this.addressTextBox.Name = "addressTextBox";
            // 
            // closeButton
            // 
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.closeButton, "closeButton");
            this.closeButton.Name = "closeButton";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // squadTableAdapter
            // 
            this.squadTableAdapter.ClearBeforeFill = true;
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // datePicker
            // 
            resources.ApplyResources(this.datePicker, "datePicker");
            this.datePicker.Name = "datePicker";
            this.datePicker.Value = new System.DateTime(2021, 6, 1, 19, 51, 29, 566);
            this.datePicker.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // timePicker
            // 
            this.timePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            resources.ApplyResources(this.timePicker, "timePicker");
            this.timePicker.Name = "timePicker";
            // 
            // InsertAccidentForm
            // 
            this.AcceptButton = this.addButton;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.CancelButton = this.closeButton;
            this.Controls.Add(this.timePicker);
            this.Controls.Add(this.datePicker);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.addressTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.accDescriptionTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.squadCodeNamePicker);
            this.Controls.Add(this.typePicker);
            this.Controls.Add(this.addButton);
            this.Name = "InsertAccidentForm";
            this.Load += new System.EventHandler(this.InsertAccidentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.accidenttypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.squadBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.squadCodeName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ComboBox typePicker;
        private accidentTypes accidentTypes;
        private System.Windows.Forms.BindingSource accidenttypeBindingSource;
        private accidentTypesTableAdapters.accident_typeTableAdapter accident_typeTableAdapter;
        private System.Windows.Forms.ComboBox squadCodeNamePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox accDescriptionTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox addressTextBox;
        private System.Windows.Forms.Button closeButton;
        private squadCodeName squadCodeName;
        private System.Windows.Forms.BindingSource squadBindingSource;
        private squadCodeNameTableAdapters.squadTableAdapter squadTableAdapter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker datePicker;
        private System.Windows.Forms.DateTimePicker timePicker;
    }
}