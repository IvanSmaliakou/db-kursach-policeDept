﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    public partial class TableReportWindow : Form
    {
        public TableReportWindow()
        {
            InitializeComponent();
        }

        private void TableReportWindow_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "fullAccDS.full_accident". При необходимости она может быть перемещена или удалена.
            this.full_accidentTableAdapter.Fill(this.fullAccDS.full_accident);

            this.reportViewer1.RefreshReport();
        }
    }
}
