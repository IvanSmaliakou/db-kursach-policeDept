﻿
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    partial class AccidentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccidentsForm));
            this.police_deptDataSet = new Kursach_PoliceDept.police_deptDataSet();
            this.accidentBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.accidentTableAdapter1 = new Kursach_PoliceDept.police_deptDataSetTableAdapters.accidentTableAdapter();
            this.tableAdapterManager1 = new Kursach_PoliceDept.police_deptDataSetTableAdapters.TableAdapterManager();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.fullaccidentBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.fullAccDataSet = new Kursach_PoliceDept.FullAccDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fullaccidentBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.full_accident = new Kursach_PoliceDept.full_accident();
            this.accidentsDataGridView = new System.Windows.Forms.DataGridView();
            this.acc_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.squad_code_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accdescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accaddressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acctypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accidentBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.full_accidentTableAdapter1 = new Kursach_PoliceDept.full_accidentTableAdapters.full_accidentTableAdapter();
            this.filterButton = new System.Windows.Forms.Button();
            this.full_accidentTableAdapter = new Kursach_PoliceDept.FullAccDataSetTableAdapters.full_accidentTableAdapter();
            this.addAccidentButton = new System.Windows.Forms.Button();
            this.fullaccidentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.full_accidentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accidentBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.accidentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.full_accidentBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.accidentBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.full_squadBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accidentBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.buttonDeleteAcc = new System.Windows.Forms.Button();
            this.loadReportButton = new System.Windows.Forms.Button();
            this.reportAccidentSelector = new System.Windows.Forms.ComboBox();
            this.deleteAccPicker = new System.Windows.Forms.ComboBox();
            this.approveDelete = new System.Windows.Forms.Button();
            this.showTableReport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.police_deptDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fullaccidentBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullAccDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullaccidentBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_accident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullaccidentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_accidentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_accidentBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_squadBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource3)).BeginInit();
            this.SuspendLayout();
            // 
            // police_deptDataSet
            // 
            this.police_deptDataSet.DataSetName = "police_deptDataSet";
            this.police_deptDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // accidentBindingSource1
            // 
            this.accidentBindingSource1.DataMember = "accident";
            this.accidentBindingSource1.DataSource = this.police_deptDataSet;
            // 
            // accidentTableAdapter1
            // 
            this.accidentTableAdapter1.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.accident_typeTableAdapter = null;
            this.tableAdapterManager1.accidentTableAdapter = this.accidentTableAdapter1;
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.patrol_carTableAdapter = null;
            this.tableAdapterManager1.policemanTableAdapter = null;
            this.tableAdapterManager1.squadTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = Kursach_PoliceDept.police_deptDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.BindingSource = this.fullaccidentBindingSource2;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(1300, 27);
            this.bindingNavigator1.TabIndex = 0;
            this.bindingNavigator1.Text = "bindingNavigator1";
            this.bindingNavigator1.RefreshItems += new System.EventHandler(this.bindingNavigator1_RefreshItems);
            // 
            // fullaccidentBindingSource2
            // 
            this.fullaccidentBindingSource2.DataMember = "full_accident";
            this.fullaccidentBindingSource2.DataSource = this.fullAccDataSet;
            // 
            // fullAccDataSet
            // 
            this.fullAccDataSet.DataSetName = "FullAccDataSet";
            this.fullAccDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(55, 24);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            this.bindingNavigatorCountItem.Click += new System.EventHandler(this.bindingNavigatorCountItem_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            this.bindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.bindingNavigatorMovePreviousItem_Click_1);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            this.bindingNavigatorMoveNextItem.Click += new System.EventHandler(this.bindingNavigatorMoveNextItem_Click);
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // fullaccidentBindingSource1
            // 
            this.fullaccidentBindingSource1.DataMember = "full_accident";
            this.fullaccidentBindingSource1.DataSource = this.full_accident;
            // 
            // full_accident
            // 
            this.full_accident.DataSetName = "full_accident";
            this.full_accident.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // accidentsDataGridView
            // 
            this.accidentsDataGridView.AllowUserToAddRows = false;
            this.accidentsDataGridView.AllowUserToDeleteRows = false;
            this.accidentsDataGridView.AutoGenerateColumns = false;
            this.accidentsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.accidentsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.acc_time,
            this.squad_code_name,
            this.accdescriptionDataGridViewTextBoxColumn,
            this.accaddressDataGridViewTextBoxColumn,
            this.acctypeDataGridViewTextBoxColumn});
            this.accidentsDataGridView.DataSource = this.fullaccidentBindingSource2;
            this.accidentsDataGridView.Location = new System.Drawing.Point(101, 143);
            this.accidentsDataGridView.Name = "accidentsDataGridView";
            this.accidentsDataGridView.ReadOnly = true;
            this.accidentsDataGridView.RowHeadersWidth = 51;
            this.accidentsDataGridView.RowTemplate.Height = 24;
            this.accidentsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.accidentsDataGridView.Size = new System.Drawing.Size(977, 230);
            this.accidentsDataGridView.TabIndex = 1;
            this.accidentsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // acc_time
            // 
            this.acc_time.DataPropertyName = "acc_time";
            this.acc_time.HeaderText = "время происшествия";
            this.acc_time.MinimumWidth = 6;
            this.acc_time.Name = "acc_time";
            this.acc_time.ReadOnly = true;
            this.acc_time.Width = 125;
            // 
            // squad_code_name
            // 
            this.squad_code_name.DataPropertyName = "squad_code_name";
            this.squad_code_name.HeaderText = "кодовое название наряда";
            this.squad_code_name.MinimumWidth = 6;
            this.squad_code_name.Name = "squad_code_name";
            this.squad_code_name.ReadOnly = true;
            this.squad_code_name.Width = 125;
            // 
            // accdescriptionDataGridViewTextBoxColumn
            // 
            this.accdescriptionDataGridViewTextBoxColumn.DataPropertyName = "acc_description";
            this.accdescriptionDataGridViewTextBoxColumn.HeaderText = "описание происшествия";
            this.accdescriptionDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.accdescriptionDataGridViewTextBoxColumn.Name = "accdescriptionDataGridViewTextBoxColumn";
            this.accdescriptionDataGridViewTextBoxColumn.ReadOnly = true;
            this.accdescriptionDataGridViewTextBoxColumn.Width = 125;
            // 
            // accaddressDataGridViewTextBoxColumn
            // 
            this.accaddressDataGridViewTextBoxColumn.DataPropertyName = "acc_address";
            this.accaddressDataGridViewTextBoxColumn.HeaderText = "адрес происшествия";
            this.accaddressDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.accaddressDataGridViewTextBoxColumn.Name = "accaddressDataGridViewTextBoxColumn";
            this.accaddressDataGridViewTextBoxColumn.ReadOnly = true;
            this.accaddressDataGridViewTextBoxColumn.Width = 125;
            // 
            // acctypeDataGridViewTextBoxColumn
            // 
            this.acctypeDataGridViewTextBoxColumn.DataPropertyName = "acc_type";
            this.acctypeDataGridViewTextBoxColumn.HeaderText = "тип происшествия";
            this.acctypeDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.acctypeDataGridViewTextBoxColumn.Name = "acctypeDataGridViewTextBoxColumn";
            this.acctypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.acctypeDataGridViewTextBoxColumn.Width = 125;
            // 
            // accidentBindingSource4
            // 
            this.accidentBindingSource4.DataMember = "accident";
            this.accidentBindingSource4.DataSource = this.police_deptDataSet;
            // 
            // full_accidentTableAdapter1
            // 
            this.full_accidentTableAdapter1.ClearBeforeFill = true;
            // 
            // filterButton
            // 
            this.filterButton.Location = new System.Drawing.Point(1049, 409);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(199, 49);
            this.filterButton.TabIndex = 2;
            this.filterButton.Text = "Фильтровать";
            this.filterButton.UseVisualStyleBackColor = true;
            this.filterButton.Click += new System.EventHandler(this.filterButton_Click);
            // 
            // full_accidentTableAdapter
            // 
            this.full_accidentTableAdapter.ClearBeforeFill = true;
            // 
            // addAccidentButton
            // 
            this.addAccidentButton.Location = new System.Drawing.Point(1049, 498);
            this.addAccidentButton.Name = "addAccidentButton";
            this.addAccidentButton.Size = new System.Drawing.Size(199, 49);
            this.addAccidentButton.TabIndex = 3;
            this.addAccidentButton.Text = "Добавить происшествие";
            this.addAccidentButton.UseVisualStyleBackColor = true;
            this.addAccidentButton.Click += new System.EventHandler(this.addAccidentButton_Click);
            // 
            // buttonDeleteAcc
            // 
            this.buttonDeleteAcc.Location = new System.Drawing.Point(1049, 587);
            this.buttonDeleteAcc.Name = "buttonDeleteAcc";
            this.buttonDeleteAcc.Size = new System.Drawing.Size(199, 49);
            this.buttonDeleteAcc.TabIndex = 4;
            this.buttonDeleteAcc.Text = "Удалить происшествие";
            this.buttonDeleteAcc.UseVisualStyleBackColor = true;
            this.buttonDeleteAcc.Click += new System.EventHandler(this.buttonDeleteAcc_Click);
            // 
            // loadReportButton
            // 
            this.loadReportButton.Location = new System.Drawing.Point(148, 572);
            this.loadReportButton.Name = "loadReportButton";
            this.loadReportButton.Size = new System.Drawing.Size(199, 35);
            this.loadReportButton.TabIndex = 5;
            this.loadReportButton.Text = "Загрузить отчет";
            this.loadReportButton.UseVisualStyleBackColor = true;
            this.loadReportButton.Click += new System.EventHandler(this.loadReportButton_Click);
            // 
            // reportAccidentSelector
            // 
            this.reportAccidentSelector.DataSource = this.fullaccidentBindingSource2;
            this.reportAccidentSelector.DisplayMember = "acc_description";
            this.reportAccidentSelector.FormattingEnabled = true;
            this.reportAccidentSelector.Location = new System.Drawing.Point(30, 523);
            this.reportAccidentSelector.Name = "reportAccidentSelector";
            this.reportAccidentSelector.Size = new System.Drawing.Size(445, 24);
            this.reportAccidentSelector.TabIndex = 6;
            // 
            // deleteAccPicker
            // 
            this.deleteAccPicker.DataSource = this.fullaccidentBindingSource2;
            this.deleteAccPicker.DisplayMember = "acc_description";
            this.deleteAccPicker.FormattingEnabled = true;
            this.deleteAccPicker.Location = new System.Drawing.Point(1049, 642);
            this.deleteAccPicker.Name = "deleteAccPicker";
            this.deleteAccPicker.Size = new System.Drawing.Size(199, 24);
            this.deleteAccPicker.TabIndex = 7;
            // 
            // approveDelete
            // 
            this.approveDelete.Location = new System.Drawing.Point(1086, 672);
            this.approveDelete.Name = "approveDelete";
            this.approveDelete.Size = new System.Drawing.Size(114, 29);
            this.approveDelete.TabIndex = 8;
            this.approveDelete.Text = "Подтвердить";
            this.approveDelete.UseVisualStyleBackColor = true;
            this.approveDelete.Click += new System.EventHandler(this.approveDelete_Click);
            // 
            // showTableReport
            // 
            this.showTableReport.Location = new System.Drawing.Point(148, 631);
            this.showTableReport.Name = "showTableReport";
            this.showTableReport.Size = new System.Drawing.Size(199, 35);
            this.showTableReport.TabIndex = 9;
            this.showTableReport.Text = "Табличный отчет";
            this.showTableReport.UseVisualStyleBackColor = true;
            this.showTableReport.Click += new System.EventHandler(this.showTableReport_Click);
            // 
            // AccidentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 850);
            this.Controls.Add(this.showTableReport);
            this.Controls.Add(this.approveDelete);
            this.Controls.Add(this.deleteAccPicker);
            this.Controls.Add(this.reportAccidentSelector);
            this.Controls.Add(this.loadReportButton);
            this.Controls.Add(this.buttonDeleteAcc);
            this.Controls.Add(this.addAccidentButton);
            this.Controls.Add(this.filterButton);
            this.Controls.Add(this.accidentsDataGridView);
            this.Controls.Add(this.bindingNavigator1);
            this.Name = "AccidentsForm";
            this.Text = "Происшествия";
            this.Load += new System.EventHandler(this.Accidents_Data_Grid_Load);
            ((System.ComponentModel.ISupportInitialize)(this.police_deptDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fullaccidentBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullAccDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullaccidentBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_accident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullaccidentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_accidentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_accidentBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.full_squadBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accidentBindingSource3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource accidentBindingSource;
        private System.Windows.Forms.BindingSource full_accidentBindingSource;
        private police_deptDataSet police_deptDataSet;
        private System.Windows.Forms.BindingSource accidentBindingSource1;
        private police_deptDataSetTableAdapters.accidentTableAdapter accidentTableAdapter1;
        private police_deptDataSetTableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.BindingSource full_accidentBindingSource1;
        private System.Windows.Forms.BindingSource accidentBindingSource2;
        private System.Windows.Forms.BindingSource full_squadBindingSource;
        private System.Windows.Forms.BindingSource accidentBindingSource3;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        public System.Windows.Forms.DataGridView accidentsDataGridView;
        private System.Windows.Forms.BindingSource accidentBindingSource4;
        private System.Windows.Forms.BindingSource accidentBindingSource5;
        private System.Windows.Forms.BindingSource fullaccidentBindingSource;
        private full_accident full_accident;
        private BindingSource fullaccidentBindingSource1;
        private full_accidentTableAdapters.full_accidentTableAdapter full_accidentTableAdapter1;
        private Button filterButton;
        private FullAccDataSet fullAccDataSet;
        private BindingSource fullaccidentBindingSource2;
        private FullAccDataSetTableAdapters.full_accidentTableAdapter full_accidentTableAdapter;
        private DataGridViewTextBoxColumn acc_time;
        private DataGridViewTextBoxColumn squad_code_name;
        private DataGridViewTextBoxColumn accdescriptionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn accaddressDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn acctypeDataGridViewTextBoxColumn;
        private Button addAccidentButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private Button buttonDeleteAcc;
        private Button loadReportButton;
        private ComboBox reportAccidentSelector;
        private ComboBox deleteAccPicker;
        private Button approveDelete;
        private Button showTableReport;
    }
}