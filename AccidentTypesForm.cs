﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    public partial class AccidentTypesForm : Form
    {
        public AccidentTypesForm()
        {
            InitializeComponent();
            applyTypeButton.Visible = false;
            typeNameTextBox.Visible = false;
        }

        private void AccidentTypesForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "accidentTypesDataSet.accident_type". При необходимости она может быть перемещена или удалена.
            this.accident_typeTableAdapter2.Fill(this.accidentTypesDataSet.accident_type);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            applyTypeButton.Visible = true;
            typeNameTextBox.Visible = true;
        }

        private void applyTypeButton_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
                ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("insert_accident_type", conn);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter type = new SqlParameter("@type", typeNameTextBox.Text);
                SqlParameter acc_id = new SqlParameter("@type_id", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(type);
                command.Parameters.Add(acc_id);

                command.ExecuteNonQuery();
                conn.Close();
                applyTypeButton.Visible = false;
                typeNameTextBox.Visible = false;
                MessageBox.Show(String.Format("Тип происшествия \"{0}\" с id={1} был добавлен", typeNameTextBox.Text, acc_id.Value), "Внимание!",
    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
                ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                foreach (DataGridViewRow row in dataGridView1.SelectedRows) {
                    SqlCommand cmd = new SqlCommand("delete from accident_type where name=@name", conn);
                    SqlParameter typeName = new SqlParameter("@name", row.Cells[0].Value as string);
                    cmd.Parameters.Add(typeName);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
                MessageBox.Show("Тип происшествия был удален", "Внимание!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
