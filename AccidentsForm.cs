﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    public partial class AccidentsForm : Form
    {
        private AccidentFilterForm accidentFilterForm;
        private InsertAccidentForm insertAccidentForm;
        private ReportWindow reportWin;
        private TableReportWindow tableReportWin;

        public AccidentsForm()
        {
            InitializeComponent();
            approveDelete.Visible = false;
            deleteAccPicker.Visible = false;
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }

        private void Accidents_Data_Grid_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "fullAccDataSet.full_accident". При необходимости она может быть перемещена или удалена.
            this.full_accidentTableAdapter.Fill(this.fullAccDataSet.full_accident);
        }

        private void full_accidentDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bindingNavigator1_RefreshItems(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bindingNavigatorMovePreviousItem_Click_1(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorCountItem_Click(object sender, EventArgs e)
        {

        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            if (accidentFilterForm == null || accidentFilterForm.IsDisposed)
            {
                accidentFilterForm = new AccidentFilterForm(accidentsDataGridView, fullaccidentBindingSource2);
            }
            accidentFilterForm.Show();
        }

        private void addAccidentButton_Click(object sender, EventArgs e)
        {
            if (insertAccidentForm == null || insertAccidentForm.IsDisposed)
            {
                insertAccidentForm = new InsertAccidentForm(this);
            }
            accidentsDataGridView.Update();
            accidentsDataGridView.Refresh();
            insertAccidentForm.Show();
        }

        private void bindingNavigatorDeleteItem_Click_1(object sender, EventArgs e)
        {
            foreach (var row in accidentsDataGridView.SelectedRows) {
                Console.WriteLine(row);
            }
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {

        }

        private void loadReportButton_Click(object sender, EventArgs e)
        {
            if (reportWin == null || reportWin.IsDisposed) {
                reportWin = new ReportWindow(reportAccidentSelector.Text);
            }
            reportWin.Show();
            reportWin.BringToFront();
        }

        private void buttonDeleteAcc_Click(object sender, EventArgs e)
        {
            if (approveDelete.Visible && deleteAccPicker.Visible) {
                approveDelete.Visible = false;
                deleteAccPicker.Visible = false;
                return;
            }
            approveDelete.Visible = true;
            deleteAccPicker.Visible = true;
        }

        private void approveDelete_Click(object sender, EventArgs e)
        {
            string connString = System.Configuration.ConfigurationManager.
                ConnectionStrings["ConnStr"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("delete from accident where description=@desc", conn);
                command.Parameters.AddWithValue("@desc", deleteAccPicker.Text);
                command.ExecuteNonQuery();
                conn.Close();
            }
            approveDelete.Visible = false;
            deleteAccPicker.Visible = false;
        }

        private void showTableReport_Click(object sender, EventArgs e)
        {
            if (tableReportWin == null || tableReportWin.IsDisposed) {
                tableReportWin = new TableReportWindow();
            }
            tableReportWin.Show();
        }
    }
}
