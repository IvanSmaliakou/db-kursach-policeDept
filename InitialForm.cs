﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach_PoliceDept
{
    public partial class InitialForm : Form
    {
        AccidentsForm acc_datagrid { get; set; }
        AccidentTypesForm acc_type_datagrid { get; set; }
        Squads_Form squad_datagrid { get; set; }
        PolicemenForm policemenForm { get; set; }
        PatrolCarForm patrolCarForm { get; set; }


        public InitialForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button_show_accidents_Click(object sender, EventArgs e)
        {
            if (acc_datagrid == null || acc_datagrid.IsDisposed)
            {
                acc_datagrid = new AccidentsForm();
            }
            acc_datagrid.Show();
        }

        private void button_show_types_Click(object sender, EventArgs e)
        {
            if (acc_type_datagrid == null || acc_type_datagrid.IsDisposed)
            {
                acc_type_datagrid = new AccidentTypesForm();
            }
            acc_type_datagrid.Show();
        }

        private void button_show_squads_Click(object sender, EventArgs e)
        {
            if (squad_datagrid == null || squad_datagrid.IsDisposed)
            {
                squad_datagrid = new Squads_Form();
            }
            squad_datagrid.Show();
        }

        private void button_show_policemen_Click(object sender, EventArgs e)
        {
            if (policemenForm == null || policemenForm.IsDisposed)
            {
                policemenForm = new PolicemenForm();
            }
            policemenForm.Show();
        }

        private void button_show_patrol_cars_Click(object sender, EventArgs e)
        {
            if (patrolCarForm == null || patrolCarForm.IsDisposed)
            {
                patrolCarForm = new PatrolCarForm();
            }
            patrolCarForm.Show();
        }
    }
}
