﻿using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kursach_PoliceDept
{
    partial class AccidentFilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonApplyFilter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.filterDateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.filterDateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.accTypeSelector = new System.Windows.Forms.ComboBox();
            this.accidenttypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accTypeNamesDataSet = new Kursach_PoliceDept.AccTypeNamesDataSet();
            this.accident_typeTableAdapter = new Kursach_PoliceDept.AccTypeNamesDataSetTableAdapters.accident_typeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.accidenttypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accTypeNamesDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonApplyFilter
            // 
            this.buttonApplyFilter.BackColor = System.Drawing.Color.Lime;
            this.buttonApplyFilter.Location = new System.Drawing.Point(320, 345);
            this.buttonApplyFilter.Name = "buttonApplyFilter";
            this.buttonApplyFilter.Size = new System.Drawing.Size(137, 54);
            this.buttonApplyFilter.TabIndex = 1;
            this.buttonApplyFilter.Text = "Применить";
            this.buttonApplyFilter.UseVisualStyleBackColor = false;
            this.buttonApplyFilter.Click += new System.EventHandler(this.buttonApplyFilter_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(229, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(336, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "Фильтрация происшествий";
            // 
            // filterDateTimePickerFrom
            // 
            this.filterDateTimePickerFrom.Location = new System.Drawing.Point(97, 125);
            this.filterDateTimePickerFrom.Name = "filterDateTimePickerFrom";
            this.filterDateTimePickerFrom.Size = new System.Drawing.Size(200, 22);
            this.filterDateTimePickerFrom.TabIndex = 3;
            this.filterDateTimePickerFrom.Value = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(51, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "С";
            // 
            // filterDateTimePickerTo
            // 
            this.filterDateTimePickerTo.Location = new System.Drawing.Point(518, 125);
            this.filterDateTimePickerTo.Name = "filterDateTimePickerTo";
            this.filterDateTimePickerTo.Size = new System.Drawing.Size(200, 22);
            this.filterDateTimePickerTo.TabIndex = 6;
            this.filterDateTimePickerTo.Value = new System.DateTime(2070, 1, 1, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(462, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "По";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(106, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Тип происшествия";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // accTypeSelector
            // 

            this.accTypeSelector.Location = new System.Drawing.Point(97, 237);
            this.accTypeSelector.Name = "accTypeSelector";
            this.accTypeSelector.Size = new System.Drawing.Size(188, 24);
            this.accTypeSelector.TabIndex = 10;
            this.accTypeSelector.SelectedIndexChanged += new System.EventHandler(this.accTypeSelector_SelectedIndexChanged);
            // 
            // accidenttypeBindingSource
            // 
            this.accidenttypeBindingSource.DataMember = "accident_type";
            this.accidenttypeBindingSource.DataSource = this.accTypeNamesDataSet;
            // 
            // accTypeNamesDataSet
            // 
            this.accTypeNamesDataSet.DataSetName = "AccTypeNamesDataSet";
            this.accTypeNamesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // accident_typeTableAdapter
            // 
            this.accident_typeTableAdapter.ClearBeforeFill = true;
            // 
            // AccidentFilterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.accTypeSelector);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.filterDateTimePickerTo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.filterDateTimePickerFrom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonApplyFilter);
            this.Name = "AccidentFilterForm";
            this.Text = "Форма фильтрации";
            this.Load += new System.EventHandler(this.AccidentFilterForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.accidenttypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accTypeNamesDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button buttonApplyFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker filterDateTimePickerFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker filterDateTimePickerTo;
        private System.Windows.Forms.Label label4;
        private Label label3;
        private ComboBox accTypeSelector;
        private AccTypeNamesDataSet accTypeNamesDataSet;
        private BindingSource accidenttypeBindingSource;
        private AccTypeNamesDataSetTableAdapters.accident_typeTableAdapter accident_typeTableAdapter;
    }
}