﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kursach_PoliceDept
{
    public partial class ReportWindow : Form
    {
        public ReportWindow(string reportDescription)
        {
            InitializeComponent();

            string connString = System.Configuration.ConfigurationManager.
                ConnectionStrings["ConnStr"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("select * from full_accident where acc_description=@desc", conn);
                command.Parameters.AddWithValue("@desc", reportDescription);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                string time = reader.GetDateTime(2).ToString("yyyy-MM-dd HH':'mm':'ss");
                Microsoft.Reporting.WinForms.ReportParameter[] rParams = new Microsoft.Reporting.WinForms.ReportParameter[]
                {
                    new Microsoft.Reporting.WinForms.ReportParameter("acc_description", reader["acc_description"] as string),
                    new Microsoft.Reporting.WinForms.ReportParameter("accTime", time),
                    new Microsoft.Reporting.WinForms.ReportParameter("accType", reader["acc_type"] as string),
                    new Microsoft.Reporting.WinForms.ReportParameter("accAddress", reader["acc_address"] as string),
                    new Microsoft.Reporting.WinForms.ReportParameter("squadCodeName", reader["squad_code_name"] as string)
                };
                reportViewer1.LocalReport.SetParameters(rParams);
                reportViewer1.RefreshReport();
                conn.Close();
            }
        }

        private void ReportWindow_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "fullAccidentReportDataSet.full_accident". При необходимости она может быть перемещена или удалена.
            this.full_accidentTableAdapter.Fill(this.fullAccidentReportDataSet.full_accident);

            this.reportViewer1.RefreshReport();
        }
    }
}
